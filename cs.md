# Världens bästa..

## Serier  

* Foxtrot
* Blueberry
* Gaston
* Asterix  
* Bloom County
 
## TV

* Hedersomnämnande till Rome, Game of Thrones och ett gäng andra..
* West Wing
* Monty Python
* House of Cards (den med Ian Richardson och ingen annan)
* Arrested Development
* The Wire

## Fotbollsspelare  

* Hernanes - Lazio
* Angelo Peruzzi - Lazio
* Paul Gascoigne - Lazio
* Pawel Nedved - Lazio
* Giuseppe Signori - Lazio
* Allesandro Nesta - Lazio
* Roberto Mancini - Lazio

## Programmeringsskurser

1. Patwic!


## Kod

	code = [80, 97, 116, 119, 105, 99, 32, 105, 115, 32, 116, 104, 101, 32, 99, 111, 111, 108, 101, 115, 116, 32, 116, 104, 105, 110, 103, 32, 115, 105, 110, 99, 101, 32, 116, 104, 101, 32, 102, 114, 105, 100, 103, 101, 33]
	print("".join([chr(x) for x in code]))

